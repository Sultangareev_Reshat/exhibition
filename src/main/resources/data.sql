insert into users(name, password, role) values ('exhibitionowner', '123', 'ADMIN');
insert into users(name, password, role) values ('participant', '123', 'PARTICIPANT');


insert into clubs (club) values ('East Coast');
insert into clubs (club) values ('West Coast');
insert into clubs (club) values ('Sun');
insert into clubs (club) values ('Simba');

insert into club_numbers (club, number) values ('East Coast', 1);
insert into club_numbers (club, number) values ('East Coast', 2);
insert into club_numbers (club, number) values ('West Coast', 3);
insert into club_numbers (club, number) values ('Sun', 4);
insert into club_numbers (club, number) values ('Sun', 5);
insert into club_numbers (club, number) values ('East Coast', 6);
insert into club_numbers (club, number) values ('Simba', 7);
insert into club_numbers (club, number) values ('Simba', 8);



insert into participant (number,name,lastname,nickname,breed,age,father,mother)  values (1, 'Alex', 'Jin', 'benny', 'Rottweiler',3,'Rottweiler','Rottweiler');
insert into participant (number,name,lastname,nickname,breed,age,father,mother)  values (2, 'Peter', 'Tukhachev','sonny', 'Rottweiler',5,'Rottweiler','Beagle');
insert into participant (number,name,lastname,nickname,breed,age,father,mother)  values (3, 'Max', 'Solovev','kenny', 'Rottweiler',12,'Rottweiler','Rottweiler');

insert into participant (number,name,lastname,nickname,breed,age,father,mother) values (4, 'Ivan','Ivanov','lenny', 'Beagle',6,'Beagle','Beagle');
insert into participant (number,name,lastname,nickname,breed,age,father,mother)  values (5, 'Michail','Tuplikov','jenny', 'Beagle',4,'Beagle','Poodle');

insert into participant (number,name,lastname,nickname,breed,age,father,mother) values (6, 'Alexey', 'Romanov','menny', 'Poodle',2,'Poodle','Poodle');
insert into participant (number,name,lastname,nickname,breed,age,father,mother)  values (7, 'Roman', 'Rurikovich','zenny', 'Poodle',4,'Poodle','Beagle');


insert into ring (ring_number, breed) values (1, 'Beagle');
insert into ring (ring_number, breed) values (2, 'Rottweiler');
insert into ring (ring_number, breed) values (3, 'Poodle');

insert into medalist (id, medal) values (1, 'gold');
insert into medalist (id, medal) values (2, 'silver');
insert into medalist (id, medal) values (4, 'gold');
insert into medalist (id, medal) values (6, 'gold');


insert into experts (name, lastname, patronymic, ring_number, club) values ('Igor', 'Safranov', 'Ivanov', 1, 'Sun');
insert into experts (name, lastname, patronymic, ring_number, club) values ('Alex', 'Peskov', 'Ruslanovich', 2, 'Simba');
insert into experts (name, lastname, patronymic, ring_number, club) values ('Pavel', 'Baranov', 'Alexandrovich', 3, 'East Coast');
insert into experts (name, lastname, patronymic, ring_number, club) values ('Jenia', 'Lin', 'Kin', 1, 'West Coast');





-- Участник – таблица(id Участника ,Клуб, Кличка, Порода, Возраст, отец, мать)
-- Номера участников(клуб, номер)
-- Эксперты(ФИО, номер ринга, клуб)
-- Ринг (номер ринга, порода собак)
-- Медалисты(id Участника , медаль)

drop table participant cascade;
drop table club_numbers cascade;
drop table experts cascade;
drop table ring cascade;
drop table medalist cascade;
drop table clubs cascade;
drop table users cascade ;

create user participant with password '123';
create user admin with password '123';

grant select on
    clubs
    to participant;
grant select on
    ring
    to participant;
grant select on
    club_numbers
    to participant;
grant select on
    participant
    to participant;
grant select on
    experts
    to participant;
grant select on
    medalist
    to participant;

grant select on
    clubs
    to exhibitionowner;
grant select on
    ring
    to exhibitionowner;
grant select, insert on
    club_numbers
    to exhibitionowner;
grant select, update on
    participant
    to exhibitionowner;
grant select, insert, delete on
    experts
    to exhibitionowner;
grant select on
    medalist
    to exhibitionowner;



create table users(
    name varchar(20) not null primary key ,
    password varchar(30) not null,
    role varchar(30) not null
);

create table clubs(
    club varchar(20) not null primary key
);
create table ring(
                     ring_number integer not null primary key ,
                     breed varchar(20) not null
);
create table club_numbers (
                              club varchar(20) not null references clubs(club),
                              number integer not null primary key
);

create table participant (
    id bigserial not null primary key  ,
    number integer not null unique references  club_numbers(number),
    name varchar(20) not null,
    lastname varchar(20) not null,
    nickname varchar(20) not null,
    breed varchar(20) not null,
    age integer not null check ( age >= 0 ),
    father varchar(20),
    mother varchar(20)
                  );

create table experts (
                         id bigserial not null primary key,
                         name varchar(20) not null,
                         lastname varchar(20) not null,
                         patronymic varchar(20) not null,
                         ring_number integer not null references ring(ring_number) ,
                         club varchar(20) not null references clubs(club)
);

create table medalist(
                     id bigint references participant(id) primary key,
                     medal varchar(20) not null
);

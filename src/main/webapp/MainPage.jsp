<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
    <link href="<c:url value="MainPage.css" />" rel="stylesheet">

    <title>All tables</title>
</head>
<body>
<form action = "toWelcomePage">
<td><input type="submit" value="Back" /></td>
</form>
<div class="mainClassRow">



        <table>
            <td>id</td>
            <td>number</td>
            <td>nickname</td>
            <td>name</td>
            <td>lastname</td>
            <td>breed</td>
            <td>age</td>
            <td>father</td>
            <td>mother</td>
            <c:forEach items="${participants}" var="person">
            <tr>
                <td><c:out value="${person.id}" /></td>
                <td><c:out value="${person.number}" /></td>
                <td><c:out value="${person.name}" /></td>
                <td><c:out value="${person.lastName}" /></td>
                <td><c:out value="${person.nickname}" /></td>
                <td><c:out value="${person.breed}" /></td>
                <td><c:out value="${person.age}" /></td>
                <td><c:out value="${person.father}" /></td>
                <td><c:out value="${person.mother}" /></td>
            </tr>
            </c:forEach>


        <table >
            <td>number</td>
            <td>club</td>
            <c:forEach items="${clubNumbers}" var="club">
                <tr>

                    <td><c:out value="${club.number}" /></td>
                    <td><c:out value="${club.club}" /></td>
                </tr>
            </c:forEach>
        </table>
                <table >
                    <td>id</td>
                    <td>name</td>
                    <td>lastname</td>
                    <td>patronymic</td>
                    <td>ring number</td>
                    <td>club</td>
                    <c:forEach items="${experts}" var="expert">
                        <tr>
                            <td><c:out value="${expert.id}" /></td>
                            <td><c:out value="${expert.name}" /></td>
                            <td><c:out value="${expert.lastname}" /></td>
                            <td><c:out value="${expert.patronymic}" /></td>
                            <td><c:out value="${expert.ring_number}" /></td>
                            <td><c:out value="${expert.club}" /></td>

                        </tr>
                    </c:forEach>
                </table>
                <table >
                    <td>id</td>
                    <td>medal</td>
                    <c:forEach items="${medalist}" var="medalist">
                        <tr>
                            <td><c:out value="${medalist.id}" /></td>
                            <td><c:out value="${medalist.medal}" /></td>
                        </tr>
                    </c:forEach>
                </table>
                <table >
                    <td>ring number</td>
                    <td>breed</td>
                    <c:forEach items="${rings}" var="ring">
                        <tr>
                            <td><c:out value="${ring.ring_number}" /></td>
                            <td><c:out value="${ring.breed}" /></td>
                        </tr>
                    </c:forEach>
                </table>
</div>
</body>
</html>

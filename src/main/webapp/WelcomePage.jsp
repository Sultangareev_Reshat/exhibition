<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<!DOCTYPE html>
<html>
<head>
    <link href="<c:url value="WelcomePage.css" />" rel="stylesheet">


    <meta charset="ISO-8859-1">

    <title>Start page</title>
</head>
<body>
<div>

    <div class="mainClassRow">
            <div>
                <div>
                    <form action="openMain">
                        <input type="submit" value="Go to tables" style="display: block;">
                    </form>
                </div>
                <div>
                    <form action="openCertificate">
                        <input type="text" name="id" placeholder="id">
                        <input type="submit" value="Get certificate" style="display: block;">
                    </form>
                </div>
                <div>
                    <form action="openDocument">
                        <input type="text" name="club" placeholder="club">
                        <input type="submit" value="Get document" style="display: block;">
                    </form>
                </div>
            </div>
        <div class="mainClassRow" style=" background-color:bisque;">

            <div class="queries">
                <form action="enterName">
                    Find dogs ring by participant number
                    <br>
                    <input type="text" name="input" placeholder="number">
                    <form action="save">

                        <input type="submit" value="Find dog" style="display: block;">
                    </form>
                </form>
                <form action="changeClub">
                    Change club
                    <br>
                    <input type="text" name="id" placeholder="number">
                    <input type="text" name="club" placeholder="club">
                    <form action="save">
                        <input type="submit" value="Change club" style="display: block;">
                    </form>
                </form>
                <form action="findClubBreeds">
                    Finding club breeds, write name of the club
                    <br>
                    <input type="text" name="clubName" placeholder="club">
                    <form action="save">
                        <input type="submit" value="Find club breeds" style="display: block;">
                    </form>
                </form>
                <form action="getClubMedals">
                    Finding club medals, write name of the club
                    <br>
                    <input type="text" name="clubName" placeholder="club">
                    <form action="save">
                        <input type="submit" value="Find club medals" style="display: block;">
                    </form>
                </form>
                <form action="getBreedExperts">
                    Finding experts for breed, write name of the breed
                    <br>
                    <input type="text" name="breed" placeholder="breed">
                    <form action="save">
                        <input type="submit" value="Find experts by the breed" placeholder="breed"
                               style="display: block;">
                    </form>
                </form>

                <form action="getDogsByExpert">
                    Finding dogs for expert
                    <br>
                    <input type="text" name="name" placeholder="name">
                    <input type="text" name="lastName" placeholder="lastname">
                    <input type="text" name="patronymic" placeholder="partonymic">

                    <form action="save">
                        <input type="submit" value="Find" style="display: block;">
                    </form>
                </form>
                <form action="deleteExpert">
                    Deleting expert
                    <br>
                    <input type="text" name="name" placeholder="name">
                    <input type="text" name="lastName" placeholder="lastname">
                    <input type="text" name="patronymic" placeholder="partonymic">

                    <form action="save">
                        <input type="submit" value="Find" style="display: block;">
                    </form>
                </form>
                <form action="insertExpert">
                    Inserting expert
                    <br>
                    <input type="text" name="name" placeholder="name">
                    <input type="text" name="lastName" placeholder="lastname">
                    <input type="text" name="patronymic" placeholder="partonymic">
                    <input type="text" name="ring_number" placeholder="ring number">
                    <input type="text" name="club" placeholder="club">

                    <form action="save">
                        <input type="submit" value="Find" style="display: block;">
                    </form>
                </form>

            </div>

            <div class="results">

                <table>
                    <td>ring number</td>
                    <td>nickname</td>
                    <c:forEach items="${dogRing}" var="dog">
                        <tr>
                            <td><c:out value="${dog.ring_number}"/></td>
                            <td><c:out value="${dog.nickname}"/></td>
                        </tr>
                    </c:forEach>
                </table>
                <table>
                    <td>Result of changing club: ${ChangeClubResult}</td>
                </table>

                <table>
                    <td>breeds of the club <c:out value="${clubBreeeds.get(0).club}"/></td>
                    <c:forEach items="${clubBreeeds}" var="club">
                        <tr>
                            <td><c:out value="${club.breed}"/></td>
                        </tr>
                    </c:forEach>
                </table>
                <table>

                    <td>Medals of the club <c:out value="${clubMedals.get(0).club}"/></td>
                    <td>${warningForMedals}</td>
                    <c:forEach items="${clubMedals}" var="club">
                        <tr>
                            <td><c:out value="${club.number}"/></td>
                            <td><c:out value="${club.medal}"/></td>
                        </tr>
                    </c:forEach>
                </table>
                <table>

                    <td>Experts for the breed <c:out value="${experts.get(0).getBreed()}"/></td>
                    <td>${warningForBreed}</td>
                    <c:forEach items="${experts}" var="experts">
                        <tr>
                            <td><c:out value="${experts.name}"/></td>
                            <td><c:out value="${experts.lastName}"/></td>
                            <td><c:out value="${experts.patronymic}"/></td>
                        </tr>
                    </c:forEach>
                </table>
                <table>

                    <%--                <td>Dogs for the expert <c:out value="${dogs.get(0).getName()}" /></td>--%>
                    <%--                <td style="border: none">${warningForMedals}</td>--%>
                    <td>dog nickname</td>
                    <td>expert name</td>
                    <td>expert lastname</td>
                    <td>expert patronymic</td>
                    <td>expert id</td>
                    <c:forEach items="${dogs}" var="dog">
                        <tr>
                            <td><c:out value="${dog.nickname}"/></td>
                            <td><c:out value="${dog.name}"/></td>
                            <td><c:out value="${dog.lastName}"/></td>
                            <td><c:out value="${dog.patronymic}"/></td>
                            <td><c:out value="${dog.id}"/></td>
                        </tr>
                    </c:forEach>
                </table>
                <table>
                    <td>Deletion result</td>
                    <td> ${deletionResult}</td>
                </table>
                <table>
                    <td>Insertion result</td>
                    <td> ${insertionResult}</td>
                </table>
            </div>
            </form>
        </div>
        <div>

        </div>
    </div>
</div>

</body>
</html>
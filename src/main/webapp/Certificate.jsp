<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="ISO-8859-1">
    <link href="<c:url value="Certificate.css" />" rel="stylesheet">

    <title>Certificate</title>
</head>
<body>
<form action = "toWelcomePage">
    <td><input type="submit" value="Back" /></td>
</form>
<div class="mainClassRow">

   Certificate<br>
    Congratulations, participant ${participantName} with dog ${dogName} in the competition of the breed ${breed} got ${medal}.

</div>
</body>
</html>

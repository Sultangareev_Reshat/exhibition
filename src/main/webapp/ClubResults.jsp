<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="ISO-8859-1">
    <link href="<c:url value="Certificate.css" />" rel="stylesheet">

    <title>Certificate</title>
</head>
<body>
<form action = "toWelcomePage">
    <td><input type="submit" value="Back" /></td>
</form>
<div class="mainClassRow">

    Results of the club ${club}
    This club had ${numberParticipants} participants which had following breeds ${breeds}.
    They won medals in following categories
    <br>

        <c:forEach items="${winners}" var="winner">
                <c:out value="${winner.breed}" />
                <c:out value="${winner.medal}" /> with nickname
                <c:out value="${winner.nickname}" /> and owner name
                <c:out value="${winner.name}" />
            <br>
        </c:forEach>

</div>
</body>
</html>

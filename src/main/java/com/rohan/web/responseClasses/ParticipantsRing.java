package com.rohan.web.responseClasses;

public class ParticipantsRing {
    Integer ring_number;
    String nickname;

    public ParticipantsRing( String nickname, Integer ring_number) {
        this.ring_number = ring_number;
        this.nickname = nickname;
    }

    public ParticipantsRing() {
    }

    public Integer getRing_number() {
        return ring_number;
    }

    public void setRing_number(Integer ring_number) {
        this.ring_number = ring_number;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}

package com.rohan.web.responseClasses;

import com.rohan.web.model.Club;
import org.springframework.beans.factory.annotation.Value;

public interface ClubNumberInterface {
        @Value(value = "#{target.number}")
        Integer getNumber();
        @Value(value = "#{target.club}")
        Club getClub();
}

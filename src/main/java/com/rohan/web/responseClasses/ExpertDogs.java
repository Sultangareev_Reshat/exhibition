package com.rohan.web.responseClasses;

public class ExpertDogs {

    String nickname;
    String name;
    String lastName;
    String patronymic;
    Integer id;

    public ExpertDogs(String nickname, String name, String lastName, String patronymic, Integer id) {
        this.nickname = nickname;
        this.name = name;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.id = id;
    }

    public ExpertDogs() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}

package com.rohan.web.responseClasses;

public class ParticipantMedalist {
    String nickname;
    String medal;

    public ParticipantMedalist(String name, String model) {
        this.nickname = name;
        this.medal = model;
    }

    public ParticipantMedalist() {
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String name) {
        this.nickname = name;
    }

    public String getMedal() {
        return medal;
    }

    public void setMedal(String model) {
        this.medal = model;
    }
}

package com.rohan.web.responseClasses;

import com.rohan.web.model.Club;

public class ClubMedals {
    Club club;
    Long number;
    String medal;

    public ClubMedals(Club club, Long number, String medal) {
        this.club = club;
        this.number = number;
        this.medal = medal;
    }

    public ClubMedals() {
    }

    public Club getClub() {
        return club;
    }

    public void setClub(Club club) {
        this.club = club;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public String getMedal() {
        return medal;
    }

    public void setMedal(String medal) {
        this.medal = medal;
    }
}

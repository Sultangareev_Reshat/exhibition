package com.rohan.web.responseClasses;

public class ExpertsBreeds {
    String breed;
    String name;
    String lastName;
    String patronymic;

    public ExpertsBreeds(String name, String lastName, String patronymic, String breed) {
        this.name = name;
        this.lastName = lastName;
        this.patronymic = patronymic;
    }

    public ExpertsBreeds() {
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }
}

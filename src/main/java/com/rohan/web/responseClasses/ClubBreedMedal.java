package com.rohan.web.responseClasses;

import com.rohan.web.model.Club;

public class ClubBreedMedal {
    Club club;
    String breed;
    String medal;
    String nickname;
    String name;

    public ClubBreedMedal(Club club, String breed, String medal, String nickname, String name) {
        this.club = club;
        this.breed = breed;
        this.medal = medal;
        this.nickname = nickname;
        this.name = name;
    }

    public ClubBreedMedal(Club club, String breed, String medal) {
        this.club = club;
        this.breed = breed;
        this.medal = medal;
    }

    public ClubBreedMedal() {
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Club getClub() {
        return club;
    }

    public void setClub(Club club) {
        this.club = club;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public String getMedal() {
        return medal;
    }

    public void setMedal(String medal) {
        this.medal = medal;
    }
}

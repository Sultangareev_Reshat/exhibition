package com.rohan.web.responseClasses;

import com.rohan.web.model.Club;

public class ClubBreeds {
    Club club;
    String breed;

    public ClubBreeds(String breed, Club club) {
        this.breed = breed;
        this.club = club;
    }

    public ClubBreeds() {
    }

    public Club getClub() {
        return club;
    }

    public void setClub(Club club) {
        this.club = club;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }
}

package com.rohan.web.responseClasses;

import com.rohan.web.model.Club;

public class ClubParticipantCount {
    Club club;
    Long count;

    public ClubParticipantCount(Club club, Long count) {
        this.club = club;
        this.count = count;
    }

    public ClubParticipantCount() {
    }

    public Club getClub() {
        return club;
    }

    public void setClub(Club club) {
        this.club = club;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }
}

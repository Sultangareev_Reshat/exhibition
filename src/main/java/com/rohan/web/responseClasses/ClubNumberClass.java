package com.rohan.web.responseClasses;

import com.rohan.web.model.Club;

public class ClubNumberClass implements ClubNumberInterface {
    Integer number;
    Club club;

    public ClubNumberClass(Integer number, Club club) {
        this.number = number;
        this.club = club;
    }

    public ClubNumberClass() {
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Club getClub() {
        return club;
    }

    public void setClub(Club club) {
        this.club = club;
    }
}

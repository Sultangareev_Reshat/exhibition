package com.rohan.web.controller;

import com.rohan.web.model.*;
import com.rohan.web.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@RestController
public class MainController {
	@Autowired
	private org.springframework.context.ApplicationContext context;
	@Autowired
	ParticipantRepository participantRepository;
	@Autowired
	ClubNumberRepository clubNumberRepository;
	@Autowired
	ExpertRepository expertRepository;
	@Autowired
	MedalistRepository medalistRepository;
	@Autowired
	RingRepository ringRepository;
	@Autowired
	CurrentUser currentUser;


	@RequestMapping("toWelcomePage")
	private ModelAndView toWelcomePage(){
		if(currentUser.getRole() != "ADMIN"){
			ModelAndView mv2 = new ModelAndView();
			mv2.setViewName("SignIn");
			return mv2;
		}
		ModelAndView mv = new ModelAndView();

		mv.setViewName("WelcomePage");
		return mv;
	}
	@RequestMapping("/main")
	private ModelAndView mainPage(){
		if(currentUser.getRole() != "ADMIN"){
			ModelAndView mv2 = new ModelAndView();
			mv2.setViewName("SignIn");
			return mv2;
		}
		ModelAndView mv = new ModelAndView();

		mv.setViewName("MainPage");


		List<Participant>  participantList = participantRepository.findAll();
		mv.addObject("participants", participantList);
		List<ClubNumber>  clubNumberList = clubNumberRepository.findAll();
		mv.addObject("clubNumbers", clubNumberList);
		List<Expert>  expertList = expertRepository.findAll();
		mv.addObject("experts", expertList);
		List <Medalist> medalistList = medalistRepository.findAll();
		mv.addObject("medalist", medalistList);
		List<Ring> ringList = ringRepository.findAll();
		mv.addObject("rings", ringList);

		return mv;
	}
}

package com.rohan.web.controller;

import com.rohan.web.model.*;
import com.rohan.web.repository.*;
import com.rohan.web.responseClasses.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;


@RestController
public class WelcomePageController {
    @Autowired
    private org.springframework.context.ApplicationContext context;
    @Autowired
    ParticipantRepository participantRepository;
    @Autowired
    MedalistRepository medalistRepository;
    @Autowired
    ClubNumberRepository clubNumberRepository;
    @Autowired
    RingRepository ringRepository;
    @Autowired
    ExpertRepository expertRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    CurrentUser currentUser;

    @PersistenceContext
    private EntityManager entityManager;

    @RequestMapping("signin")
    private ModelAndView signin(@RequestParam("name") String name, @RequestParam("password") String password) {
        ModelAndView mv = new ModelAndView();
        User user = userRepository.findUser(name, password);
        if(user == null){
			mv.setViewName("SignIn");
			return mv;
		}
        if (user.getRole().equals("ADMIN")) {
            currentUser.setRole("ADMIN");
            mv.setViewName("WelcomePage");
        } else if (user.getRole().equals("PARTICIPANT")) {
			List<Participant> participantList = participantRepository.findAll();
			mv.addObject("participants", participantList);
			List<ClubNumber> clubNumberList = clubNumberRepository.findAll();
			mv.addObject("clubNumbers", clubNumberList);
			List<Expert> expertList = expertRepository.findAll();
			mv.addObject("experts", expertList);
			List<Medalist> medalistList = medalistRepository.findAll();
			mv.addObject("medalist", medalistList);
			List<Ring> ringList = ringRepository.findAll();
			mv.addObject("rings", ringList);
            mv.setViewName("MainPage");
        }
        return mv;
    }


    @RequestMapping(value = "/", method = RequestMethod.GET)
    private ModelAndView welcome() {
        ModelAndView mv = new ModelAndView();
        String nVal = "a";
        nVal = nVal + null;
        mv.setViewName("SignIn");
        return mv;
    }

    @RequestMapping("openMain")
    private ModelAndView action() {
        if (currentUser.getRole() != "ADMIN") {
            ModelAndView mv2 = new ModelAndView();
            mv2.setViewName("SignIn");
            return mv2;
        }
        ModelAndView mv = new ModelAndView();
        mv.setViewName("MainPage");


        List<Participant> participantList = participantRepository.findAll();
        mv.addObject("participants", participantList);
        List<ClubNumber> clubNumberList = clubNumberRepository.findAll();
        mv.addObject("clubNumbers", clubNumberList);
        List<Expert> expertList = expertRepository.findAll();
        mv.addObject("experts", expertList);
        List<Medalist> medalistList = medalistRepository.findAll();
        mv.addObject("medalist", medalistList);
        List<Ring> ringList = ringRepository.findAll();
        mv.addObject("rings", ringList);

        return mv;
    }

    //
    @RequestMapping("openDocument")
    private ModelAndView openDocument(@RequestParam("club") String club) {
        if (currentUser.getRole() != "ADMIN") {
            ModelAndView mv2 = new ModelAndView();
            mv2.setViewName("SignIn");
            return mv2;
        }
        ModelAndView mv = new ModelAndView();
        mv.setViewName("ClubResults");
        Club cl = new Club(club);

        ClubParticipantCount countPartic = participantRepository.getNumberOfParticipants(cl);
        List<ClubBreedMedal> clubBreedMedals = participantRepository.getClubBreedMedals(cl);
        List<ClubBreeds> clubBreeds = participantRepository.getClubBreeds(cl);

        mv.addObject("club", club);
        mv.addObject("numberParticipants", countPartic.getCount().toString());
        String breeds = "";
        for (int i = 0; i < clubBreeds.size(); ++i) {
            if (!breeds.equals("")) {
                breeds += ", ";
            }
            breeds += clubBreeds.get(i).getBreed();
        }

        mv.addObject("breeds", breeds);
        mv.addObject("winners", clubBreedMedals);


        return mv;
    }


    @RequestMapping("openCertificate")
    private ModelAndView openCertificate(@RequestParam("id") String id) {
        if (currentUser.getRole() != "ADMIN") {
            ModelAndView mv2 = new ModelAndView();
            mv2.setViewName("SignIn");
            return mv2;
        }
        ModelAndView mv = new ModelAndView();
        Integer participantID = Integer.parseInt(id);
        mv.setViewName("Certificate");
        if (participantID != null) {

            Optional<Participant> participant1 = participantRepository.findById(participantID);
            Optional<Medalist> medalist = medalistRepository.findById(participantID);

            Participant participant = new Participant();
            participant1.ifPresent(participant2 -> {
                participant.setNickname(participant2.getNickname());
                participant.setBreed(participant2.getBreed());
                participant.setName(participant2.getName());
            });

            Medalist medalistM = new Medalist();

            medalist.ifPresent(medalist1 -> medalistM.setMedal(medalist1.getMedal()));
            if (medalistM != null && participant != null) {
                mv.addObject("participantName", participant.getName());
                mv.addObject("dogName", participant.getNickname());
                mv.addObject("breed", participant.getBreed());
                mv.addObject("medal", medalistM.getMedal());
            }


        }


        return mv;
    }

    //	@PutMapping("/participants/{id}")
//	public ResponseEntity<Participant> updateParticipant(@PathVariable(value = "id") Long participantID,
//														 @RequestBody Participant participantDetails) throws ResourceNotFoundException {
//		Participant participant = participantRepository.findById(participantID)
//				.orElseThrow(() -> new ResourceNotFoundException("Participant not found for this id :: " + participantID));
//
//		participant.setClub(participantDetails.getClub());
//		participant.setNickname(participantDetails.getNickname());
//		participant.setBreed(participantDetails.getBreed());
//		participant.setAge(participantDetails.getAge());
//		participant.setFather(participantDetails.getFather());
//		participant.setMother(participantDetails.getMother());
//
//		final Participant updatedParticipant = participantRepository.save(participant);
//		return ResponseEntity.ok(updatedParticipant);
//	}
    @RequestMapping("findClubBreeds")
    private ModelAndView findClubBreeds(@RequestParam("clubName") String clubName) {
        if (currentUser.getRole() != "ADMIN") {
            ModelAndView mv2 = new ModelAndView();
            mv2.setViewName("SignIn");
            return mv2;
        }
        ModelAndView mv = new ModelAndView();
        mv.setViewName("WelcomePage");
        if (clubName.isEmpty())
            return mv;

        Club club = new Club(clubName);

        List<ClubBreeds> resCB = participantRepository.getClubBreeds(club);

        if (!resCB.isEmpty())
            mv.addObject("clubBreeeds", resCB);

        return mv;
    }

    @RequestMapping("getBreedExperts")
    private ModelAndView getBreedExperts(@RequestParam("breed") String breed) {
        if (currentUser.getRole() != "ADMIN") {
            ModelAndView mv2 = new ModelAndView();
            mv2.setViewName("SignIn");
            return mv2;
        }
        ModelAndView mv = new ModelAndView();
        mv.setViewName("WelcomePage");
        if (breed.isEmpty())
            return mv;


        List<ExpertsBreeds> resCB = participantRepository.getExpertBreeds(breed);

        if (!resCB.isEmpty()) {
            resCB.get(0).setBreed(breed);
            mv.addObject("experts", resCB);
        } else {
            //Club club, Long number, String medal
            mv.addObject("warningForBreeds", "Couldn't find any experts for breed "
                    + breed);
        }

        return mv;
    }

    @RequestMapping("insertExpert")
    //String name, String lastName, String patronymic, Integer ring_number, String club
    private ModelAndView insertExpert(@RequestParam("name") String name, @RequestParam("lastName") String lastName,
                                      @RequestParam("patronymic") String patronymic, @RequestParam("ring_number") String ring_number, @RequestParam("club") String club) {
        if (currentUser.getRole() != "ADMIN") {
            ModelAndView mv2 = new ModelAndView();
            mv2.setViewName("SignIn");
            return mv2;
        }
        ModelAndView mv = new ModelAndView();
        mv.setViewName("WelcomePage");
        if (name.isEmpty() && lastName.isEmpty() && patronymic.isEmpty() &&
                ring_number.isEmpty() && club.isEmpty())
            return mv;

        Integer ringNum = Integer.parseInt(ring_number);


        Integer res = participantRepository.insertExpert(name, lastName, patronymic, ringNum, club);
        if (res == 1) {
            //deletionResult
            mv.addObject("insertionResult", "Successfully inserted expert " + name + " " + lastName + " " + patronymic);

        } else {
            mv.addObject("insertionResult", "Couldn't insert expert " + name + " " + lastName + " " + patronymic);
        }
        return mv;
    }

    @RequestMapping("deleteExpert")
    private ModelAndView deleteExpert(@RequestParam("name") String name, @RequestParam("lastName") String lastName, @RequestParam("patronymic") String patronymic) {
        if (currentUser.getRole() != "ADMIN") {
            ModelAndView mv2 = new ModelAndView();
            mv2.setViewName("SignIn");
            return mv2;
        }
        ModelAndView mv = new ModelAndView();
        mv.setViewName("WelcomePage");
        if (name.isEmpty() || lastName.isEmpty() || patronymic.isEmpty())
            return mv;


        Integer res = participantRepository.deleteExpert(name, lastName, patronymic);
        if (res == 1) {
            //deletionResult
            mv.addObject("deletionResult", "Successfully removed expert " + name + " " + lastName + " " + patronymic);

        } else {
            mv.addObject("deletionResult", "Couldn't remove expert " + name + " " + lastName + " " + patronymic);
        }

//		if(!resCB.isEmpty()) {
//			mv.addObject("Deletionresult", resCB);
//		} else {
//			//Club club, Long number, String medal
//			mv.addObject("warningForDogs", "Couldn't find any dogs for expert "
//					+ name);
//		}

        return mv;
        //			List<ExpertDogs> ed = participantRepository.getExpertDogs(nameExpert);
    }

    @RequestMapping("getDogsByExpert")
    private ModelAndView getDogsByExpert(@RequestParam("name") String name, @RequestParam("lastName") String lastName, @RequestParam("patronymic") String patronymic) {
        if (currentUser.getRole() != "ADMIN") {
            ModelAndView mv2 = new ModelAndView();
            mv2.setViewName("SignIn");
            return mv2;
        }
        ModelAndView mv = new ModelAndView();
        mv.setViewName("WelcomePage");
        if (name.isEmpty() || lastName.isEmpty() || patronymic.isEmpty())
            return mv;


        List<ExpertDogs> resCB = participantRepository.getExpertDogs(name, lastName, patronymic);

        if (!resCB.isEmpty()) {
            mv.addObject("dogs", resCB);
        } else {
            //Club club, Long number, String medal
            mv.addObject("warningForDogs", "Couldn't find any dogs for expert "
                    + name);
        }

        return mv;
    }

    @RequestMapping("getClubMedals")
    private ModelAndView getClubMedals(@RequestParam("clubName") String clubName) {
        if (currentUser.getRole() != "ADMIN") {
            ModelAndView mv2 = new ModelAndView();
            mv2.setViewName("SignIn");
            return mv2;
        }
        ModelAndView mv = new ModelAndView();
        mv.setViewName("WelcomePage");
        if (clubName.isEmpty())
            return mv;
        Club club = new Club(clubName);

        List<ClubMedals> resCB = participantRepository.getClubMedals(club);

        if (!resCB.isEmpty()) {
            mv.addObject("clubMedals", resCB);
        } else {
            //Club club, Long number, String medal
            mv.addObject("warningForMedals", "Couldn't find any medals for club "
                    + clubName);
        }

        return mv;
    }

    @RequestMapping("changeClub")
    private ModelAndView changeClub(@RequestParam("id") String id, @RequestParam("club") String club) {
        if (currentUser.getRole() != "ADMIN") {
            ModelAndView mv2 = new ModelAndView();
            mv2.setViewName("SignIn");
            return mv2;
        }
        ModelAndView mv = new ModelAndView();
        mv.setViewName("WelcomePage");

        if (!id.isEmpty() && !club.isEmpty()) {
            Club cl = new Club(club);
            Integer num = Integer.parseInt(id);

            Integer newNum = -1;

            Participant participant = participantRepository.findByNumber(num);
            if (participant == null) {
                mv.addObject("ChangeClubResult", "Cannot find participant with id " + id);

                return mv;
            }

            List<ClubNumberInterface> res = participantRepository.getClubNumberClass();

            for (ClubNumberInterface array : res) {
                Integer resN = array.getNumber();
                Club resC = array.getClub();
                if (resC.getClub().equals(cl.getClub()) && newNum == -1) {
                    newNum = resN;
                }
            }
            String output = "";
            if (newNum != -1) {
                output = "Changed successfully " + participant.getId() + " is now in the club " + cl.getClub().toString();
                participant.setNumber(newNum);
                final Participant updatedParticipant = participantRepository.save(participant);
            } else if (participant != null) {
                newNum = participantRepository.findMaxNumberFromClubNumbers() + 1;
                ClubNumber clubNums = new ClubNumber(newNum, cl);
                final ClubNumber updatedClubNum = clubNumberRepository.save(clubNums);
                participant.setNumber(newNum);
                final Participant updatedParticipant = participantRepository.save(participant);
                output = "Changed successfully " + participant.getId() + " is now in the club " +
                        cl.getClub().toString() + " added new number " + newNum;
            }

            mv.addObject("ChangeClubResult", output);

        }
        return mv;
    }


    @RequestMapping("enterName")
    private ModelAndView enterName(@RequestParam("input") String name) {
        if (currentUser.getRole() != "ADMIN") {
            ModelAndView mv2 = new ModelAndView();
            mv2.setViewName("SignIn");
            return mv2;
        }
        ModelAndView mv = new ModelAndView();
        mv.setViewName("WelcomePage");
        if (name != "") {
            Integer num = Integer.parseInt(name);

//			List<ParticipantMedalist> resM = participantRepository.showParticipantsMedals();
//
            List<ParticipantsRing> dogList = participantRepository.getDogRing(num);

            Club club = new Club("East Coast");
            String breed = "Beagle";
            String nameExpert = "Igor";


            List<ClubBreeds> resCB = participantRepository.getClubBreeds(club);
            List<ClubMedals> cm = participantRepository.getClubMedals(club);
            List<ExpertsBreeds> eb = participantRepository.getExpertBreeds(breed);


            if (dogList.size() > 0)
                mv.addObject("dogRing", dogList);

        }
        return mv;
    }


}

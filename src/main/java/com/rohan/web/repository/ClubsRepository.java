package com.rohan.web.repository;

import com.rohan.web.model.Club;
import com.rohan.web.model.ClubNumberClass;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClubsRepository extends JpaRepository<Club, ClubNumberClass> {
}

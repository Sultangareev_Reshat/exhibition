package com.rohan.web.repository;

import com.rohan.web.model.ClubNumber;
import com.rohan.web.model.Expert;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExpertRepository extends JpaRepository<Expert, Integer> {
}

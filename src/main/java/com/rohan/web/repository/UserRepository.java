package com.rohan.web.repository;

import com.rohan.web.model.Ring;
import com.rohan.web.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends JpaRepository<User, Integer> {
    @Query("select new com.rohan.web.model.User(u.name, u.password, u.role) from User u where u.name = :name and u.password = :password")
    public User findUser(@Param("name") String name, @Param("password") String password);
}

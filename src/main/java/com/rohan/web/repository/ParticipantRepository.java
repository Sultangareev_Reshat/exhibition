package com.rohan.web.repository;

import com.rohan.web.model.Club;
import com.rohan.web.model.ClubNumber;
import com.rohan.web.model.Participant;
import com.rohan.web.responseClasses.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.OneToOne;
import java.util.List;

@Transactional
public interface ParticipantRepository  extends JpaRepository<Participant, Integer> {
    Participant findByNickname(String nickname);
    Participant findByNumber(Integer number);

    @Query("SELECT new com.rohan.web.responseClasses.ParticipantsRing(c.nickname , p.ring_number) FROM Participant c JOIN c.ring p where c.id = ?1")
    public List<ParticipantsRing> getDogRing(Integer id);
    @Query("Select distinct new com.rohan.web.responseClasses.ClubBreeds( c.breed, p.club) from Participant c join c.clubNumber p on c.id = p.number where p.club = :club")
    public List<ClubBreeds> getClubBreeds(@Param("club") Club club);
    //select count(medal), medal from club_numbers inner join medalist on medalist.id = club_numbers.number where club = 'East Coast' group by medal;
    //select name, lastname,patronymic from ring inner join experts on ring.ring_number = experts.ring_number where breed = 'Beagle';
    @Query("Select distinct new com.rohan.web.responseClasses.ExpertsBreeds( c.name, c.lastname, c.patronymic, p.breed) from Expert c join c.ring p on c.ring_number = p.ring_number where p.breed = :breed")
    public List<ExpertsBreeds> getExpertBreeds(@Param("breed") String breed);
    //select count(medal), medal from club_numbers inner join medalist on medalist.id = club_numbers.number where club = 'East Coast' group by medal;
    @Query("Select distinct new com.rohan.web.responseClasses.ClubMedals( c.club, count(p.medal), p.medal) from ClubNumber c join c.medalist p on c.number = p.id where c.club = :club group by p.medal, c.club")
    public List<ClubMedals> getClubMedals(@Param("club") Club club);
    //select experts.name, participant.nickname from participant inner join ring on ring.breed = participant.breed inner join experts on experts.ring_number = ring.ring_number where experts.name = 'Igor';
    @Query("Select distinct new com.rohan.web.responseClasses.ExpertDogs(c.nickname, e.name, e.lastname, e.patronymic, e.id) from Participant c join c.ring p on c.breed = p.breed join Expert e on e.ring_number = p.ring_number" +
            " where e.name = :name and e.lastname = :lastName and e.patronymic = :patronymic")
    public List<ExpertDogs> getExpertDogs(@Param("name") String name, @Param("lastName") String lastName, @Param("patronymic") String patronymic);
    //select club, club_numbers.number from club_numbers except select club, club_numbers.number from club_numbers join participant on club_numbers.number = participant.number;
    @Query(value = "select club, club_numbers.number from club_numbers except select club, club_numbers.number from club_numbers join participant on club_numbers.number = participant.number", nativeQuery = true)
    public List<ClubNumberInterface> getClubNumberClass();
    @Modifying
    @Query("delete from Expert e where e.name = :name and e.lastname = :lastName and e.patronymic = :patronymic")
    public Integer deleteExpert(@Param("name") String name, @Param("lastName") String lastName, @Param("patronymic") String patronymic);
    //insert into experts (name, lastname, patronymic, ring_number, club) values ('Jenia', 'Lin', 'Kin', 1, 'West Coast');
    @Modifying
    @Query(value = "insert into experts(name, lastname, patronymic, ring_number, club) values (?1, ?2, ?3, ?4, ?5) ", nativeQuery = true)
    public Integer insertExpert(String name, String lastName, String patronymic, Integer ring_number, String club);


    //select club, count(participant.number) from participant join club_numbers on club_numbers.number = participant.number where club = '' group by club_numbers.club;
    //number of participants
    //ClubParticipantCount
    @Query("select new com.rohan.web.responseClasses.ClubParticipantCount(c.club, count(c.number)) from Participant p join ClubNumber c on c.number = p.number where c.club = :club group by c.club")
    ClubParticipantCount getNumberOfParticipants(@Param("club") Club club);
    //select club, breed from participant join club_numbers on club_numbers.number = participant.number where club = 'East Coast' group by club_numbers.club, breed;
    //breeds of the club - done before
    //select club, breed, medal from participant join club_numbers on participant.number = club_numbers.number join medalist on medalist.id = participant.id where club = 'East Coast' group by breed, medal, club;
    //get club winning breeds
    @Query("select new com.rohan.web.responseClasses.ClubBreedMedal(c.club, p.breed, m.medal, p.nickname, p.name) from Participant p join ClubNumber c on p.number = c.number join Medalist m on m.id = p.id where c.club = :club group by p.breed, m.medal, c.club, p.nickname, p.name")
    public List<ClubBreedMedal> getClubBreedMedals(@Param("club") Club club);

    @Query(value = "select max(p.number) from ClubNumber p")
    Integer findMaxNumberFromClubNumbers();
//ExpertDogs
}

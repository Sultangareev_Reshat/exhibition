package com.rohan.web.repository;

import com.rohan.web.model.Club;
import com.rohan.web.model.ClubNumber;
import com.rohan.web.model.ClubNumberClass;
import com.rohan.web.model.Participant;
import com.rohan.web.responseClasses.ClubBreeds;
import com.rohan.web.responseClasses.ClubMedals;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ClubNumberRepository extends JpaRepository<ClubNumber, Integer>{
    ClubNumber findByNumber(Integer number);


}

package com.rohan.web.repository;

import com.rohan.web.model.ClubNumber;
import com.rohan.web.model.Medalist;
import com.rohan.web.model.Participant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedalistRepository extends JpaRepository<Medalist, Integer> {


}

package com.rohan.web.model;

import javax.persistence.*;

@Entity
@Table(name = "medalist")
public class Medalist {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    String medal;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id")
    private Participant participant;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Medalist(String medal) {
        this.medal = medal;
    }

    public Medalist() {
    }

    public String getMedal() {
        return medal;
    }

    public void setMedal(String medal) {
        this.medal = medal;
    }
}

//    create table medalist(
//        id bigint references participant(id) primary key,
//    medal varchar(20) not null
//        );
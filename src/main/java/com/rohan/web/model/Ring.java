package com.rohan.web.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "ring")
public class Ring implements Serializable {
    @Id
    Integer ring_number;
    String breed;

    @OneToMany(targetEntity = Participant.class, mappedBy = "ring")
    private Set<Participant> participants = new HashSet<>();


    public Ring(Integer ring_number, String breed) {
        this.ring_number = ring_number;
        this.breed = breed;
    }

    public Ring() {
    }

    public Integer getRing_number() {
        return ring_number;
    }

    public void setRing_number(Integer ring_number) {
        this.ring_number = ring_number;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }
}

//    create table ring(
//        ring_number integer not null,
//        breed varchar(20) not null,
//        primary key (ring_number, breed)
//        );

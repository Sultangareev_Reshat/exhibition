package com.rohan.web.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "club_numbers")
public class ClubNumber implements Serializable {


    @Id
    @Column(name = "number")
    Integer number;

    @ManyToOne
    @JoinColumn(name = "club")
    private Club club;

    @OneToOne(targetEntity = Medalist.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "number", referencedColumnName = "id", insertable = false, updatable = false)
    private Medalist medalist;

    @OneToOne(targetEntity = Participant.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "number", referencedColumnName = "number", insertable = false, updatable = false)
    private Participant participant;

//    @OneToOne(mappedBy = "club_numbers", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
//    private Participant participant;

    public Club getClub() {
        return club;
    }

    public void setClub(String clubs) {
        this.club = club;
    }

    public ClubNumber(Integer number, Club club) {
        this.number = number;
        this.club = club;
    }

    public ClubNumber() {
    }


    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }



}


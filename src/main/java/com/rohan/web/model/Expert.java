package com.rohan.web.model;

import javax.persistence.*;

@Entity
@Table(name = "Experts")
public class Expert {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    Integer id;
    String name;
    String lastname;
    String patronymic;
    Integer ring_number;
    String club;

    @ManyToOne(targetEntity = Ring.class, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "ring_number", referencedColumnName = "ring_number", insertable = false, updatable = false)
    private Ring ring;

    public Expert(Integer id, String name, String lastname, String patronymic, Integer ring_number, String club) {
        this.id = id;
        this.name = name;
        this.lastname = lastname;
        this.patronymic = patronymic;
        this.ring_number = ring_number;
        this.club = club;
    }

    public Expert() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public Integer getRing_number() {
        return ring_number;
    }

    public void setRing_number(Integer ring_number) {
        this.ring_number = ring_number;
    }

    public String getClub() {
        return club;
    }

    public void setClub(String club) {
        this.club = club;
    }
}
//    create table experts (
//        id bigserial not null primary key,
//        name varchar(20) not null,
//        lastname varchar(20) not null,
//        patronymic varchar(20) not null,
//        ring_number integer not null,
//        club varchar(20) not null
//        );
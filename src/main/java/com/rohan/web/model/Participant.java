package com.rohan.web.model;

import javax.persistence.*;

import org.springframework.stereotype.Component;


@Entity
@Table(name = "participant")
public class Participant {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	private Integer number;

	private String name;
	@Column(name = "lastname")
	private String lastName;

	private String nickname;
	private String breed;
	private Integer age;
	private String father;
	private String mother;

	@OneToOne(mappedBy = "participant", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Medalist medalist;

	@OneToOne(targetEntity = ClubNumber.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "number", referencedColumnName = "number", insertable = false, updatable = false)
	private ClubNumber clubNumber;

	@ManyToOne(targetEntity = Ring.class, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "breed", referencedColumnName = "breed", insertable = false, updatable = false)
	private Ring ring;

	public Participant(String club, String nickname, String breed, Integer age, String father, String mother) {
		this.nickname = nickname;
		this.breed = breed;
		this.age = age;
		this.father = father;
		this.mother = mother;
	}

	public Participant(Integer id, Integer number, String name, String lastName, String nickname, String breed, Integer age, String father, String mother) {
		this.id = id;
		this.number = number;
		this.name = name;
		this.lastName = lastName;
		this.nickname = nickname;
		this.breed = breed;
		this.age = age;
		this.father = father;
		this.mother = mother;
	}

	public Participant() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public long getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getBreed() {
		return breed;
	}

	public void setBreed(String breed) {
		this.breed = breed;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getFather() {
		return father;
	}

	public void setFather(String father) {
		this.father = father;
	}

	public String getMother() {
		return mother;
	}

	public void setMother(String mother) {
		this.mother = mother;
	}
}

package com.rohan.web.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "clubs")
public class Club {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
            @Column(name = "club")
    String club;


    @OneToMany(mappedBy = "club")
    private Set<ClubNumber> clubNumbers = new HashSet<>();

    public Club(String club) {
        this.club = club;
    }

    public Club() {
    }

    public String getClub() {
        return club;
    }

    public void setClub(String club) {
        this.club = club;
    }


    @Override
    public String toString() {
        return club ;
    }
}

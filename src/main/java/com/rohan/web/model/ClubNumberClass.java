package com.rohan.web.model;

import java.io.Serializable;

public class ClubNumberClass implements Serializable {
    String club;

    Integer number;


    public ClubNumberClass(String club, Integer number) {
        this.club = club;
        this.number = number;
    }

    public ClubNumberClass() {
    }

    public String getClub() {
        return club;
    }

    public void setClub(String club) {
        this.club = club;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }
}
